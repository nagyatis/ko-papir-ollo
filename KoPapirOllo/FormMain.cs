﻿using System;
using System.Drawing;
using System.Windows.Forms;
using System.IO;

namespace KoPapirOllo
{
    public enum Uzemmodok { Start, Jatek };
    public enum Szereplok { Macok, Mogyoro };

    public partial class FormMain : Form
    {
        System.Media.SoundPlayer miau = new System.Media.SoundPlayer(Properties.Resources.miau);
        System.Media.SoundPlayer ugatas = new System.Media.SoundPlayer(Properties.Resources.ugatas);
        System.Media.SoundPlayer kukorekolas = new System.Media.SoundPlayer(Properties.Resources.kukorekolas);
        System.Media.SoundPlayer kot = new System.Media.SoundPlayer(Properties.Resources.kot);

        private Uzemmodok uzemmod;
        private Szereplok szereplo;
        private Valaszok jatekosValasz;
        private Valaszok szereploValasz;
        private Random random;
        private int jatekosPontjai;
        private int szereploPontjai;

        public FormMain()
        {
            InitializeComponent();
        }

        private void FormMain_Load(object sender, EventArgs e)
        {
            random = new Random();
        }

        private void startUzemmod()
        {
            if (uzemmod == Uzemmodok.Start) return;
            uzemmod = Uzemmodok.Start;
            this.BackgroundImage = Properties.Resources.hatter_nyito;
            panelPontok.Visible = false;
            pictureBoxKo.Visible = false;
            pictureBoxPapir.Visible = false;
            pictureBoxOllo.Visible = false;
            buttonUjJatek.Visible = false;
            pictureBoxMacok.Visible = true;
            pictureBoxMogyoro.Visible = true;
            pictureBoxKukori.Location = new Point(297, 365);
        }

        private void jatekUzemmod()
        {
            if (uzemmod == Uzemmodok.Jatek) return;
            uzemmod = Uzemmodok.Jatek;
            szereploPontjai = 0;
            jatekosPontjai = 0;
            pontFrissites();
            this.BackgroundImage = Properties.Resources.hatter_jatek;
            panelPontok.Visible = true;
            pictureBoxKo.Visible = true;
            pictureBoxPapir.Visible = true;
            pictureBoxOllo.Visible = true;
            buttonUjJatek.Visible = true;
            pictureBoxMacok.Visible = (szereplo == Szereplok.Macok);
            pictureBoxMogyoro.Visible = (szereplo == Szereplok.Mogyoro);

            for (int i = 365; i >= 135; i = i - 10)
            {
                pictureBoxKukori.Top = i;
                this.Refresh();
                System.Threading.Thread.Sleep(1);
            }

            for (int i = 297; i >= 182; i = i - 10)
            {
                pictureBoxKukori.Left = i;
                this.Refresh();
                System.Threading.Thread.Sleep(1);
            }
        }

        private void pontFrissites()
        {
            labelSzereploNeve.Text = (szereplo == Szereplok.Macok)  ? "Macok pontjai:" : "Mogyoró pontjai:";
            labelJatekosPontjai.Text = jatekosPontjai.ToString();
            labelSzereploPontjai.Text = szereploPontjai.ToString();
        }

        private void buttonNewGame_Click(object sender, EventArgs e)
        {
            startUzemmod();
        }

        private void pictureBoxMacok_Click(object sender, EventArgs e)
        {
            if (uzemmod == Uzemmodok.Start)
            {
                szereplo = Szereplok.Macok;
                macokNyavog("Szívesen játszom Veled! 😆");
                jatekUzemmod();
            }
        }

        private void pictureBoxMogyoro_Click(object sender, EventArgs e)
        {
            if (uzemmod == Uzemmodok.Start)
            {
                szereplo = Szereplok.Mogyoro;
                mogyoroUgat("Biztos velem szeretnél játszani? 🙄");
                jatekUzemmod();
            }
        }

        private void pictureBoxKo_Click(object sender, EventArgs e)
        {
            jatekosValasz = Valaszok.Ko;
            valasz_Click();
        }

        private void pictureBoxPapir_Click(object sender, EventArgs e)
        {
            jatekosValasz = Valaszok.Papir;
            valasz_Click();
        }

        private void pictureBoxOllo_Click(object sender, EventArgs e)
        {
            jatekosValasz = Valaszok.Ollo;
            valasz_Click();
        }

        private void valasz_Click()
        {
            szereploValasz = (Valaszok)random.Next(0, 3);
            string szereploValaszSzoveggel = "";

            switch (szereploValasz)
            {
                case Valaszok.Ko:
                    szereploValaszSzoveggel = "Kő";
                    break;
                case Valaszok.Papir:
                    szereploValaszSzoveggel = "Papír";
                    break;
                case Valaszok.Ollo:
                    szereploValaszSzoveggel = "Olló";
                    break;
            }

            if (szereplo == Szereplok.Macok)
            {
                macokNyavog(szereploValaszSzoveggel, 250, false);
            }
            else
            {
                mogyoroUgat(szereploValaszSzoveggel, 250, false);
            }

            Nyertesek? nyertes = Kombinaciok.kiNyert(jatekosValasz, szereploValasz);
            if (nyertes == Nyertesek.Jatekos)
            {
                kukoriKot("Te nyertél! 😀");
                jatekosPontjai++;
            }
            if (nyertes == Nyertesek.Szereplo)
            {
                if (szereplo == Szereplok.Macok)
                {
                    macokNyavog("NYERTEM! 😆");
                }
                else
                {
                    mogyoroUgat("Csak nem én nyertem? 🙄");
                }
                szereploPontjai++;
            }
            if (nyertes == Nyertesek.Dontetlen)
            {
                kukoriKot("Döntetlen.");
            }
            pontFrissites();
        }

        private void macokNyavog(String uzenet = "", int ido = 500, bool hang = true)
        {
            if (hang)
                miau.Play();
            Point eredeti = pictureBoxMacok.Location;
            pictureBoxMacok.Image = Properties.Resources.macok_orul;
            if (uzenet != "")
            {
                labelMacok.Text = uzenet;
                labelMacok.Visible = true;
            }
            this.Refresh();
            System.Threading.Thread.Sleep(ido);
            labelMacok.Visible = false;
            pictureBoxMacok.Image = Properties.Resources.macok_ul;
            pictureBoxMacok.Location = eredeti;
            this.Refresh();
        }

        private void mogyoroUgat(String uzenet = "", int ido = 500, bool hang = true)
        {
            if (hang)
                ugatas.Play();
            Point eredeti = pictureBoxMogyoro.Location;
            pictureBoxMogyoro.Image = Properties.Resources.mogyoro_gondolkodik;
            if (uzenet != "")
            {
                labelMogyoro.Text = uzenet;
                labelMogyoro.Visible = true;
            }
            this.Refresh();
            System.Threading.Thread.Sleep(ido);
            labelMogyoro.Visible = false;
            pictureBoxMogyoro.Image = Properties.Resources.mogyoro_all;
            pictureBoxMogyoro.Location = eredeti;
            this.Refresh();
        }

        private void kukoriKot(String uzenet = "", int ido = 500)
        {
            kot.Play();
            if (uzenet != "")
            {
                labelKukori.Text = uzenet;
                labelKukori.Visible = true;
            }
            this.Refresh();
            System.Threading.Thread.Sleep(ido);
            labelKukori.Visible = false;
            this.Refresh();
        }

        private void kukoriKukorekol(String uzenet = "", int ido = 500)
        {
            kukorekolas.Play();
            Point eredeti = pictureBoxKukori.Location;
            pictureBoxKukori.Image = Properties.Resources.kukori_kukorekol;
            if (uzenet != "")
            {
                labelKukori.Text = uzenet;
                labelKukori.Visible = true;
            }
            this.Refresh();
            System.Threading.Thread.Sleep(ido);
            labelKukori.Visible = false;
            pictureBoxKukori.Image = Properties.Resources.kukori_szembol;
            pictureBoxKukori.Location = eredeti;
            this.Refresh();
        }

        private void buttonTESZT_Click(object sender, EventArgs e)
        {
            //
        }

        private void FormMain_Shown(object sender, EventArgs e)
        {
            MessageBox.Show(
                "Üdvözöllek a Kő-Papír-Olló játékban!\n\n" +
                "Kattintással válaszd ki, hogy Macokkal (a macskával) vagy Mogyoróval (a kutyával) játszani. Kukori (a kakas) a játékmester, segít a játék folyamán.\n" +
                "Az ellenfél kiválasztását követően indul a játék, a jobb felső sarokban található kő, papír és olló ikonokkal tippelhetsz.\n\n" +
                "Kellemes szórakozást!",
                "Kő-Papír-Olló",
                MessageBoxButtons.OK,
                MessageBoxIcon.Information
                );
            macokNyavog("Szia! Én Macok vagyok!", 2000);
            mogyoroUgat("Halihó! Én pedig Mogyoró!", 2000);
            kukoriKukorekol("Szia! Én Kukori vagyok, a játékmester.\nMacokkal vagy Mogyoróval szeretnél játszani?\nKérlek kattintással válassz!", 3000);
        }

        private void pictureBoxKukori_Click(object sender, EventArgs e)
        {
            if (uzemmod == Uzemmodok.Start)
            {
                kukoriKot("Sajnos velem nem játszhatsz. 😒", 1000);
            }
        }
    }
}
