﻿
namespace KoPapirOllo
{
    partial class FormMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormMain));
            this.pictureBoxMacok = new System.Windows.Forms.PictureBox();
            this.pictureBoxMogyoro = new System.Windows.Forms.PictureBox();
            this.pictureBoxKukori = new System.Windows.Forms.PictureBox();
            this.buttonUjJatek = new System.Windows.Forms.Button();
            this.panelPontok = new System.Windows.Forms.Panel();
            this.labelSzereploPontjai = new System.Windows.Forms.Label();
            this.labelJatekosPontjai = new System.Windows.Forms.Label();
            this.labelSzereploNeve = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.pictureBoxKo = new System.Windows.Forms.PictureBox();
            this.pictureBoxPapir = new System.Windows.Forms.PictureBox();
            this.pictureBoxOllo = new System.Windows.Forms.PictureBox();
            this.buttonTESZT = new System.Windows.Forms.Button();
            this.labelMacok = new System.Windows.Forms.Label();
            this.labelKukori = new System.Windows.Forms.Label();
            this.labelMogyoro = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxMacok)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxMogyoro)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxKukori)).BeginInit();
            this.panelPontok.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxKo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxPapir)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxOllo)).BeginInit();
            this.SuspendLayout();
            // 
            // pictureBoxMacok
            // 
            this.pictureBoxMacok.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.pictureBoxMacok.BackColor = System.Drawing.Color.Transparent;
            this.pictureBoxMacok.Image = global::KoPapirOllo.Properties.Resources.macok_ul;
            this.pictureBoxMacok.Location = new System.Drawing.Point(30, 496);
            this.pictureBoxMacok.Name = "pictureBoxMacok";
            this.pictureBoxMacok.Size = new System.Drawing.Size(216, 142);
            this.pictureBoxMacok.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.pictureBoxMacok.TabIndex = 3;
            this.pictureBoxMacok.TabStop = false;
            this.pictureBoxMacok.Click += new System.EventHandler(this.pictureBoxMacok_Click);
            // 
            // pictureBoxMogyoro
            // 
            this.pictureBoxMogyoro.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.pictureBoxMogyoro.BackColor = System.Drawing.Color.Transparent;
            this.pictureBoxMogyoro.Image = global::KoPapirOllo.Properties.Resources.mogyoro_all;
            this.pictureBoxMogyoro.Location = new System.Drawing.Point(605, 435);
            this.pictureBoxMogyoro.Name = "pictureBoxMogyoro";
            this.pictureBoxMogyoro.Size = new System.Drawing.Size(254, 218);
            this.pictureBoxMogyoro.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.pictureBoxMogyoro.TabIndex = 4;
            this.pictureBoxMogyoro.TabStop = false;
            this.pictureBoxMogyoro.Click += new System.EventHandler(this.pictureBoxMogyoro_Click);
            // 
            // pictureBoxKukori
            // 
            this.pictureBoxKukori.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.pictureBoxKukori.BackColor = System.Drawing.Color.Transparent;
            this.pictureBoxKukori.Image = global::KoPapirOllo.Properties.Resources.kukori_szembol;
            this.pictureBoxKukori.Location = new System.Drawing.Point(297, 365);
            this.pictureBoxKukori.Name = "pictureBoxKukori";
            this.pictureBoxKukori.Size = new System.Drawing.Size(214, 284);
            this.pictureBoxKukori.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.pictureBoxKukori.TabIndex = 5;
            this.pictureBoxKukori.TabStop = false;
            this.pictureBoxKukori.Click += new System.EventHandler(this.pictureBoxKukori_Click);
            // 
            // buttonUjJatek
            // 
            this.buttonUjJatek.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonUjJatek.Location = new System.Drawing.Point(400, 600);
            this.buttonUjJatek.Name = "buttonUjJatek";
            this.buttonUjJatek.Size = new System.Drawing.Size(100, 33);
            this.buttonUjJatek.TabIndex = 6;
            this.buttonUjJatek.Text = "Új játék";
            this.buttonUjJatek.UseVisualStyleBackColor = true;
            this.buttonUjJatek.Visible = false;
            this.buttonUjJatek.Click += new System.EventHandler(this.buttonNewGame_Click);
            // 
            // panelPontok
            // 
            this.panelPontok.BackColor = System.Drawing.Color.White;
            this.panelPontok.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panelPontok.Controls.Add(this.labelSzereploPontjai);
            this.panelPontok.Controls.Add(this.labelJatekosPontjai);
            this.panelPontok.Controls.Add(this.labelSzereploNeve);
            this.panelPontok.Controls.Add(this.label6);
            this.panelPontok.Location = new System.Drawing.Point(12, 12);
            this.panelPontok.Name = "panelPontok";
            this.panelPontok.Size = new System.Drawing.Size(221, 58);
            this.panelPontok.TabIndex = 9;
            this.panelPontok.Visible = false;
            // 
            // labelSzereploPontjai
            // 
            this.labelSzereploPontjai.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.labelSzereploPontjai.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.labelSzereploPontjai.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.labelSzereploPontjai.ForeColor = System.Drawing.Color.White;
            this.labelSzereploPontjai.Location = new System.Drawing.Point(139, 31);
            this.labelSzereploPontjai.Name = "labelSzereploPontjai";
            this.labelSzereploPontjai.Size = new System.Drawing.Size(70, 18);
            this.labelSzereploPontjai.TabIndex = 3;
            this.labelSzereploPontjai.Text = "0";
            this.labelSzereploPontjai.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // labelJatekosPontjai
            // 
            this.labelJatekosPontjai.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.labelJatekosPontjai.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.labelJatekosPontjai.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.labelJatekosPontjai.ForeColor = System.Drawing.Color.White;
            this.labelJatekosPontjai.Location = new System.Drawing.Point(139, 8);
            this.labelJatekosPontjai.Name = "labelJatekosPontjai";
            this.labelJatekosPontjai.Size = new System.Drawing.Size(70, 18);
            this.labelJatekosPontjai.TabIndex = 2;
            this.labelJatekosPontjai.Text = "0";
            this.labelJatekosPontjai.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // labelSzereploNeve
            // 
            this.labelSzereploNeve.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.labelSzereploNeve.Location = new System.Drawing.Point(8, 31);
            this.labelSzereploNeve.Name = "labelSzereploNeve";
            this.labelSzereploNeve.Size = new System.Drawing.Size(125, 18);
            this.labelSzereploNeve.TabIndex = 1;
            this.labelSzereploNeve.Text = "<Szereplő> pontjai:";
            this.labelSzereploNeve.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label6
            // 
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label6.Location = new System.Drawing.Point(8, 8);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(125, 18);
            this.label6.TabIndex = 0;
            this.label6.Text = "Saját pontjaim:";
            this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // pictureBoxKo
            // 
            this.pictureBoxKo.BackColor = System.Drawing.Color.Transparent;
            this.pictureBoxKo.Image = global::KoPapirOllo.Properties.Resources.ko;
            this.pictureBoxKo.Location = new System.Drawing.Point(535, 12);
            this.pictureBoxKo.Name = "pictureBoxKo";
            this.pictureBoxKo.Size = new System.Drawing.Size(100, 100);
            this.pictureBoxKo.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBoxKo.TabIndex = 13;
            this.pictureBoxKo.TabStop = false;
            this.pictureBoxKo.Visible = false;
            this.pictureBoxKo.Click += new System.EventHandler(this.pictureBoxKo_Click);
            // 
            // pictureBoxPapir
            // 
            this.pictureBoxPapir.BackColor = System.Drawing.Color.Transparent;
            this.pictureBoxPapir.Image = global::KoPapirOllo.Properties.Resources.papir;
            this.pictureBoxPapir.Location = new System.Drawing.Point(654, 12);
            this.pictureBoxPapir.Name = "pictureBoxPapir";
            this.pictureBoxPapir.Size = new System.Drawing.Size(100, 100);
            this.pictureBoxPapir.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBoxPapir.TabIndex = 14;
            this.pictureBoxPapir.TabStop = false;
            this.pictureBoxPapir.Visible = false;
            this.pictureBoxPapir.Click += new System.EventHandler(this.pictureBoxPapir_Click);
            // 
            // pictureBoxOllo
            // 
            this.pictureBoxOllo.BackColor = System.Drawing.Color.Transparent;
            this.pictureBoxOllo.Image = global::KoPapirOllo.Properties.Resources.ollo;
            this.pictureBoxOllo.Location = new System.Drawing.Point(772, 12);
            this.pictureBoxOllo.Name = "pictureBoxOllo";
            this.pictureBoxOllo.Size = new System.Drawing.Size(100, 100);
            this.pictureBoxOllo.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBoxOllo.TabIndex = 15;
            this.pictureBoxOllo.TabStop = false;
            this.pictureBoxOllo.Visible = false;
            this.pictureBoxOllo.Click += new System.EventHandler(this.pictureBoxOllo_Click);
            // 
            // buttonTESZT
            // 
            this.buttonTESZT.Location = new System.Drawing.Point(239, 12);
            this.buttonTESZT.Name = "buttonTESZT";
            this.buttonTESZT.Size = new System.Drawing.Size(75, 23);
            this.buttonTESZT.TabIndex = 16;
            this.buttonTESZT.Text = "TESZT";
            this.buttonTESZT.UseVisualStyleBackColor = true;
            this.buttonTESZT.Visible = false;
            this.buttonTESZT.Click += new System.EventHandler(this.buttonTESZT_Click);
            // 
            // labelMacok
            // 
            this.labelMacok.AutoSize = true;
            this.labelMacok.BackColor = System.Drawing.Color.White;
            this.labelMacok.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.labelMacok.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.labelMacok.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.labelMacok.Location = new System.Drawing.Point(178, 446);
            this.labelMacok.Name = "labelMacok";
            this.labelMacok.Padding = new System.Windows.Forms.Padding(5);
            this.labelMacok.Size = new System.Drawing.Size(128, 36);
            this.labelMacok.TabIndex = 17;
            this.labelMacok.Text = "labelMacok";
            this.labelMacok.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.labelMacok.Visible = false;
            // 
            // labelKukori
            // 
            this.labelKukori.AutoSize = true;
            this.labelKukori.BackColor = System.Drawing.Color.White;
            this.labelKukori.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.labelKukori.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.labelKukori.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.labelKukori.Location = new System.Drawing.Point(321, 260);
            this.labelKukori.Name = "labelKukori";
            this.labelKukori.Padding = new System.Windows.Forms.Padding(5);
            this.labelKukori.Size = new System.Drawing.Size(126, 36);
            this.labelKukori.TabIndex = 18;
            this.labelKukori.Text = "labelKukori";
            this.labelKukori.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.labelKukori.Visible = false;
            // 
            // labelMogyoro
            // 
            this.labelMogyoro.AutoSize = true;
            this.labelMogyoro.BackColor = System.Drawing.Color.White;
            this.labelMogyoro.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.labelMogyoro.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.labelMogyoro.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.labelMogyoro.Location = new System.Drawing.Point(517, 396);
            this.labelMogyoro.Name = "labelMogyoro";
            this.labelMogyoro.Padding = new System.Windows.Forms.Padding(5);
            this.labelMogyoro.Size = new System.Drawing.Size(149, 36);
            this.labelMogyoro.TabIndex = 19;
            this.labelMogyoro.Text = "labelMogyoro";
            this.labelMogyoro.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.labelMogyoro.Visible = false;
            // 
            // FormMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = global::KoPapirOllo.Properties.Resources.hatter_nyito;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(884, 661);
            this.Controls.Add(this.labelMogyoro);
            this.Controls.Add(this.labelKukori);
            this.Controls.Add(this.labelMacok);
            this.Controls.Add(this.buttonTESZT);
            this.Controls.Add(this.pictureBoxOllo);
            this.Controls.Add(this.pictureBoxPapir);
            this.Controls.Add(this.pictureBoxKo);
            this.Controls.Add(this.panelPontok);
            this.Controls.Add(this.buttonUjJatek);
            this.Controls.Add(this.pictureBoxKukori);
            this.Controls.Add(this.pictureBoxMogyoro);
            this.Controls.Add(this.pictureBoxMacok);
            this.DoubleBuffered = true;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "FormMain";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Kő-Papír-Olló - Copyright © 2021, Nagy Attila Sándor (AIBWCZ) 😉";
            this.Load += new System.EventHandler(this.FormMain_Load);
            this.Shown += new System.EventHandler(this.FormMain_Shown);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxMacok)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxMogyoro)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxKukori)).EndInit();
            this.panelPontok.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxKo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxPapir)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxOllo)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.PictureBox pictureBoxMacok;
        private System.Windows.Forms.PictureBox pictureBoxMogyoro;
        private System.Windows.Forms.PictureBox pictureBoxKukori;
        private System.Windows.Forms.Button buttonUjJatek;
        private System.Windows.Forms.Panel panelPontok;
        private System.Windows.Forms.Label labelSzereploPontjai;
        private System.Windows.Forms.Label labelJatekosPontjai;
        private System.Windows.Forms.Label labelSzereploNeve;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.PictureBox pictureBoxKo;
        private System.Windows.Forms.PictureBox pictureBoxPapir;
        private System.Windows.Forms.PictureBox pictureBoxOllo;
        private System.Windows.Forms.Button buttonTESZT;
        private System.Windows.Forms.Label labelMacok;
        private System.Windows.Forms.Label labelKukori;
        private System.Windows.Forms.Label labelMogyoro;
    }
}

