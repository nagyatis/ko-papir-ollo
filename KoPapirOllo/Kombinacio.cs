﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KoPapirOllo
{
    public enum Valaszok { Ko, Papir, Ollo };
    public enum Nyertesek { Jatekos, Szereplo, Dontetlen };

    class Kombinacio
    {
        protected Valaszok? _jatekosValasz = null;
        protected Valaszok? _szereploValasz = null;
        protected Nyertesek? _nyertes = null;

        public Valaszok? JatekosValasz
        {
            get
            {
                if (_jatekosValasz is null)
                    throw new Exception("A JatekosValasz (még) nincs beállítva!");
                return _jatekosValasz;
            }
            set
            {
                _jatekosValasz = value;
            }
        }

        public Valaszok? SzereploValasz
        {
            get
            {
                if (_szereploValasz is null)
                    throw new Exception("A SzereploValasz (még) nincs beállítva!");
                return _szereploValasz;
            }
            set
            {
                _szereploValasz = value;
            }
        }

        public Nyertesek? Nyertes
        {
            get
            {
                if (_nyertes is null)
                    throw new Exception("A Nyertes (még) nincs beállítva!");
                return _nyertes;
            }
            set
            {
                if ((_jatekosValasz is null) || (_szereploValasz is null))
                    throw new Exception("Előbb a JatekosValasz és SzereploValasz tulajdonságokat kell beállítani!");
                if ((_jatekosValasz == _szereploValasz) && (value != Nyertesek.Dontetlen))
                    throw new Exception("Megegyező JatekosValasz és SzereploValasz tulajdonságok esetén a Nyertes értéke csak Nyertesek.Dontetlen lehet!");
                if ((_jatekosValasz != _szereploValasz) && (value == Nyertesek.Dontetlen))
                    throw new Exception("Nem egyező JatekosValasz és SzereploValasz tulajdonságok esetén a Nyertes értéke nem lehet Nyertesek.Dontetlen!");
                _nyertes = value;
            }
        }
                
        private Kombinacio(Valaszok jatekosValasz, Valaszok szereploValasz, Nyertesek nyertes)
        {
            this.JatekosValasz = jatekosValasz;
            this.SzereploValasz = szereploValasz;
            this.Nyertes = nyertes;
        }

        public static Kombinacio ujKombinacio(Valaszok jatekosValasz, Valaszok szereploValasz, Nyertesek nyertes)
        {
            if ((jatekosValasz == szereploValasz) && (nyertes != Nyertesek.Dontetlen))
                throw new ArgumentException("Megegyező JatekosValasz és SzereploValasz tulajdonságok esetén a Nyertes értéke csak Nyertesek.Dontetlen lehet!");
            if ((jatekosValasz != szereploValasz) && (nyertes == Nyertesek.Dontetlen))
                throw new ArgumentException("Nem egyező JatekosValasz és SzereploValasz tulajdonságok esetén a Nyertes értéke nem lehet Nyertesek.Dontetlen!");
            return new Kombinacio(jatekosValasz, szereploValasz, nyertes);
        }
    }
}
