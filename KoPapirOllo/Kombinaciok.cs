﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KoPapirOllo
{
    static class Kombinaciok
    {
        private static readonly List<Kombinacio> kombinaciok = new List<Kombinacio>();

        static Kombinaciok()
        {
            kombinaciok.Add(Kombinacio.ujKombinacio(Valaszok.Ko, Valaszok.Ko, Nyertesek.Dontetlen));
            kombinaciok.Add(Kombinacio.ujKombinacio(Valaszok.Ko, Valaszok.Papir, Nyertesek.Szereplo));
            kombinaciok.Add(Kombinacio.ujKombinacio(Valaszok.Ko, Valaszok.Ollo, Nyertesek.Jatekos));
            kombinaciok.Add(Kombinacio.ujKombinacio(Valaszok.Papir, Valaszok.Ko, Nyertesek.Jatekos));
            kombinaciok.Add(Kombinacio.ujKombinacio(Valaszok.Papir, Valaszok.Papir, Nyertesek.Dontetlen));
            kombinaciok.Add(Kombinacio.ujKombinacio(Valaszok.Papir, Valaszok.Ollo, Nyertesek.Szereplo));
            kombinaciok.Add(Kombinacio.ujKombinacio(Valaszok.Ollo, Valaszok.Ko, Nyertesek.Szereplo));
            kombinaciok.Add(Kombinacio.ujKombinacio(Valaszok.Ollo, Valaszok.Papir, Nyertesek.Jatekos));
            kombinaciok.Add(Kombinacio.ujKombinacio(Valaszok.Ollo, Valaszok.Ollo, Nyertesek.Dontetlen));
        }

        public static Nyertesek? kiNyert(Valaszok jatekosValasz, Valaszok szereploValasz)
        {
            return
                kombinaciok.Find(x => (x.JatekosValasz == jatekosValasz) && (x.SzereploValasz == szereploValasz)).Nyertes;
        }
    }
}
